
"""Description: Prepare BSRN file station.
                - Read BSRN file after quality checks
                - compute Net LW
                - resample daily or monthly, if not is instantaneous
                - save it to output file

   How it is work: python work_stations.file -f in_file -d_in dir in files
                                             -f_out out_file -d_out dir out
                                             -tp type_data

   Author: Nuno Simoes
"""


import glob
from pathlib import Path
import argparse
import logging
import pandas as pd


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


def prepare_bsrn(in_file, out_file, type_data):
    """Description: main - prepare bsrn file.
                    - read csv file
                    - compute net lw
                    - resample if daily or monthly
                    - save it to out_file

    Parameters:
    -----------
    in_file :: station file after quality checks
    out_file :: output filename
    type_data :: instantaneous, daily or monthly
    """
    logger.info("Input: %s; Output: %s; Type: %s; "
                % (in_file, out_file, type_data))
    # variables to compute net lw
    lwd = 'LWD [W/m**2]'
    lwu = 'LWU [W/m**2]'
    nlw = 'NLW'

    # Read file
    d = pd.read_csv(
        in_file, delimiter="\t", parse_dates=[0],
        index_col=0, header=_get_header_n_lines(in_file)
    )
    # Compute net longwave
    d[nlw] = d[lwd] - d[lwu]

    # compute daily or monthly if variables are true
    if type_data == "daily":
        d = d.resample('D').mean()
        logging.info("Daily resample was performed.")
    if type_data == "monthly":
        d = d.resample('D').mean().resample('M').mean()
        logging.info("Monthly resample was performed.")

    # Save to file
    d.to_csv(out_file, index=True)


def _get_header_n_lines(filename):
    """Description: get header number lines filled by data description.
    """
    with open(filename, "r") as f:
        return [idx for idx, i in enumerate(f) if i == "*/\n"][0] + 1


def _is_valid_file(parser, arg):
    """Description: Test if input conf file is valid.
                    This function is used with argparser.
    """
    return arg if Path(arg).is_file() else\
        parser.error("The file %s does not exist! " % arg)


def _is_valid_dir(parser, arg):
    """Description: Test if input dir or output dir is valid.
                    This function is used with argparser.
    """
    return arg if Path(arg).is_dir() else\
        parser.error("The directory %s does not exist! " % arg)


def _log_level_string_to_int(log_level_string):
    """Description: Test log leve string given in argparse.
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def _prepare_args():
    """Description: prepare arguments from argparse
    """
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f_in", "--input_file",
        help="input filename (BSRN)", metavar='FILE',
        type=lambda x: _is_valid_file(parser, x)
    )
    parser.add_argument(
        "-d_in", "--dir_in",
        help="input directory - used when you have multiple files",
        type=lambda x: _is_valid_dir(parser, x)
    )
    parser.add_argument(
        "-f_out", "--output_file",
        help="output filename", metavar="FILE")
    parser.add_argument(
        "-d_out", "--dir_out",
        help="output directory - used when you have multiple files",
        type=lambda x: _is_valid_dir(parser, x)
    )
    parser.add_argument(
        "-tp", "--type", required=True,
        help="instantaneous or daily or monthly", type=str)
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                        ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()
    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)

    # Info
    logging.info(
        "Input: %s; Dir_in: %s; Output: %s; Dir_Out: %s; Kind: %s "
        % (args.input_file, args.dir_in, args.output_file,
           args.dir_out, args.type)
    )

    # call prepare_bsrn function
    if args.input_file:
        # logger info
        logger.info("Just One File!")
        # Call prepare_bsrn
        prepare_bsrn(args.input_file, args.output_file, args.type)
    # if there are multiple files
    elif args.dir_in and args.dir_out:
        # get files and check is there are files
        logger.info("Multiple Files!")
        files = glob.glob(args.dir_in + "*combined*")
        if files:
            # Loop Over files and call prepare_bsrn
            str_nslf = "_with_NSLF.txt"
            [prepare_bsrn(
                f_in,
                args.dir_out + f_in.split("/")[-1].split(".")[0]+str_nslf,
                args.type) for f_in in files]
        else:
            logger.error("There are not files in dir_in %s !" % args.dir_in)


if __name__ == "__main__":
    _prepare_args()
