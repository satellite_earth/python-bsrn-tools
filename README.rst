python-bsrn-tools
-----------------

This package has multiple scripts to manipulate BSRN files.


Installation:
-------------

### Requirements

    - Linux
    - Python 3.3 or up


Usage:
------

Please read the Note_BSRN before you use the scripts.

**1. compute_nlw_from_bsrn.py** 

    This script computes the net longwave from each bsrn file (or multiple files), join the new value to total dataframe and save it to a new file. Please check the description for more information.

    *One File:* python compute_nlw_from_bsrn.py -f_in input_file -f_out output_file -tp "instantaneous"

    *Multiple Files:* python compute_nlw_from_bsrn.py -d_in dir_bsrn_files -d_out dir_outputs -tp "instantaneous"

    *Daily means 1 File:* python compute_nlw_from_bsrn.py -f_in input_file -f_out output_file -tp "daily"

    *Monthly mean 1 File:* python compute_nlw_from_bsrn.py -f_in input_file -f_out output_file -tp "monthly"


Note_BSRN:
----------

Please see the following steps in order to get the BSRN files ready to use in this package.

*### Download BSRN data*
    - Donwload BSRN-ToolBox: https://bsrn.awi.de/software/bsrn-toolbox/
    - Check the data availability: https://bsrn.awi.de/data/data-retrieval-via-pangaea/
*### Quality Check*
    - BSRN-ToolBox -> Select Folder (data from BSRN .dat)
    - BSRN-ToolBox -> Quality Check -> BSRN Recommended V2.0 -> Select the three checks (Physically possible limits; Extremely rare limits; Comparisons) -> Select Original data and Quality Codes and One Combined output file.
    - The output file Station_Timeslot_QC_combined will be used in this package.


Contribution:
-------------

Please open an issue first to discuss what do you like to change.
